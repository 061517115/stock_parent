//package com.itheima.stock;
//
//import org.junit.jupiter.api.Test;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.data.redis.core.*;
//
//import java.util.Iterator;
//import java.util.List;
//import java.util.Set;
//
///**
// * @author by itheima
// * @Date 2021/12/30
// * @Description
// */
//@SpringBootTest
//public class TestRedis {
//
//    @Autowired
//    private RedisTemplate<String,String> redisTemplate;
//
//    @Autowired
//    private RedisTemplate<String,Object> redisTemplate1;
//
//    @Test
//    public void test01(){
//
////        操作字符串数据类型
//        ValueOperations<String, String> stringStringValueOperations = redisTemplate.opsForValue();
//
//        //存入值
//        redisTemplate.opsForValue().set("myname","zhangsan");
//        //获取值
//        String myname = redisTemplate.opsForValue().get("myname");
//        System.out.println(myname);
//
//        ListOperations<String, String> listOperations = redisTemplate.opsForList();
//        listOperations.leftPushAll("mylist","b","c","d","6");
//        Long size = listOperations.size("mylist");
//
//        int lSize = size.intValue();
//        for (int i = 0; i < lSize; i++) {
//            //出队列
//            String element = (String) listOperations.rightPop("mylist");
//            System.out.println(element);
//        }
/////**
//// * 操作Hash类型数据
////*/
//        HashOperations<String, Object, Object> hashOperations = redisTemplate1.opsForHash();
//        hashOperations.put("002","name","xiaoming");
//        hashOperations.put("002","age","20");
//        hashOperations.put("002","address","bj");
//
//        String age = (String) hashOperations.get("002", "address");
//        System.out.println(age);
//        System.out.println("======================================");
//        //获得hash结构中的所有字段
//        Set keys = hashOperations.keys("002");
//        for (Object key : keys) {
//            System.out.println(key);
//        }
//
////获得hash结构中的所有值
//        List values = hashOperations.values("002");
//        for (Object value : values) {
//            System.out.println(value);
//        }
//        ListOperations<String, Object> listOperations1 = redisTemplate1.opsForList();
//
//        //存值
//        listOperations.leftPush("mylist","a");
//        listOperations.leftPushAll("mylist","b","c","d");
//
//        //取值              注意分别 R 和 L
//        List<String> mylist = listOperations.range("mylist", 0, -1);
//        for (String value : mylist) {
//            System.out.println(value);
//
//        }
//        System.out.println("==============================================================");
//
//        ZSetOperations zSetOperations = redisTemplate.opsForZSet();
//
//        //存值
//        zSetOperations.add("myZset","a",10.0);
//        zSetOperations.add("myZset","b",19.0);
//        zSetOperations.add("myZset","c",12.0);
//        zSetOperations.add("myZset","a",13.0);
//
//        //取值
//        Set<String> myZset = zSetOperations.range("myZset", 0, -1);
//        for (String s : myZset) {
//            System.out.println(s);
//        }
//        System.out.println("分数分割线+====================");
//        //修改分数
//        zSetOperations.incrementScore("myZset","b",20.0);
//
//        //取值
//        myZset = zSetOperations.range("myZset", 0, -1);
//        for (String s : myZset) {
//            System.out.println(s+"拿到的健");
//
//        }
////             zSetOperations.rangeByScoreWithScores()   等效鱼   降级
//        Set<ZSetOperations.TypedTuple> myZset1 = zSetOperations.rangeWithScores("myZset", 0, -1);
//        myZset1.forEach(item -> {
//            Double score = item.getScore();
//            System.out.println("score = " + score);
//            String value = (String)item.getValue();
//            System.out.println("value = " + value+"分数");
//        });
//
//
////        Set set1 = zSetOperations.rangeByScoreWithScores("myZset", 9, 20);
////        System.out.println(set1+"拿到的set");
////        for (Object o : set1) {
////            System.out.println("o = " + o);
////
////        }
//
//
//
//
//    }
//
//}