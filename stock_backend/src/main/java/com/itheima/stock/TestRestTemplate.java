//package com.itheima.stock;
//
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.client.RestTemplate;
//
///**
// * @author by itheima
// * @Date 2022/1/1
// * @Description
// */
//@SpringBootTest
//public class TestRestTemplate {
//
//    @Autowired
//    private RestTemplate restTemplate;
//
//    /**
//     * 测试get请求携带url参数，访问外部接口
//     */
//    @Test
//    public void test01(){
//        String url="http://localhost:6666/account/getByUserNameAndAddress?userName=itheima&address=shanghai";
//        /*
//          参数1：url请求地址
//          参数2：请求返回的数据类型
//         */
//        ResponseEntity<String> result = restTemplate.getForEntity(url, String.class);
//        //获取响应头
//        HttpHeaders headers = result.getHeaders();
//        System.out.println(headers.toString());
//        //响应状态码
//        int statusCode = result.getStatusCodeValue();
//        System.out.println(statusCode);
//        //响应数据
//        String respData = result.getBody();
//        System.out.println(respData);
//    }
//}
