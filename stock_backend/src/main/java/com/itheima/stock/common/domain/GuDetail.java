package com.itheima.stock.common.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class GuDetail {
    private String code;
    private String trade;
    private String business;
    private String name;

}
