package com.itheima.stock.common.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author by itheima
 * @Date 2022/2/28
 * @Description 个股日K数据封装
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class secondDomain {

    //当前日期
    private String date;

    /*
  jdbc:bigint--->java:long
 */
    private Long tradeAmt;          //最新交易量
    /**
     * 日期，eg:202201280809
     */

     /*
        jdbc:decimal --->java:BigDecimal
     */
    /**
     * 当前交易总金额
     */
    private BigDecimal tradeVol;


    //当前价格
    private BigDecimal tradePrice;



}