package com.itheima.stock.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class Shiroconfig {
    //shirofilterfactoryBean
             @Bean(name ="shiroFilterFactoryBean")
            public ShiroFilterFactoryBean shiroFilterFactoryBean(@Qualifier("securityManager1") DefaultWebSecurityManager defaultWebSecurityManager){
//设置安全管理器
                ShiroFilterFactoryBean sBean = new ShiroFilterFactoryBean();
                sBean.setSecurityManager(defaultWebSecurityManager);

//添加过滤器
//          anon 无需认证就可以访问
                 //   authc  认证了才可以访问
//                 // user 记住我才能用
//                 //perms y有资源
//                //role

                 Map<String,String> fp = new LinkedHashMap<>();

       //授权    没有授权正常情况下会自动跳转到 未授权页面

                 fp.put("/user/add","perms[user:add]");


//未授权页面
                 sBean.setUnauthorizedUrl("/noauth");

                 //   fp.put("/user/add","authc");
                          fp.put("/user/update","authc");
                               sBean.setFilterChainDefinitionMap(fp);
                               //设置登录请求页面
                               sBean.setLoginUrl("/toLogin");
                                return sBean;
            }
        //dafaultwebsecritymanger            需要指定哪个类 realm
    @Bean(name ="securityManager1")
public DefaultWebSecurityManager getdefaultWebSecurityManager(@Qualifier("userRealm") UserRealm userRealm ){
    DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
    securityManager.setRealm(userRealm);
    return  securityManager;
    //需要关联下面的realm

}



    //创建 realam对象
    @Bean(name = "userRealm")    //表示自己写的配置类被spring托管了  1
 public UserRealm userRealm(){


     return  new UserRealm();
 }
 //整合ShiroDialect
    @Bean
    public ShiroDialect getShiroDialect(){
                 return new ShiroDialect();
    }

}
