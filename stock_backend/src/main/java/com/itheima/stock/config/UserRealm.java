package com.itheima.stock.config;



import com.itheima.stock.pojo.SysUser;
import com.itheima.stock.pojo.User;
import com.itheima.stock.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

//自定义的UserRealm

public class UserRealm extends AuthorizingRealm {
    //授权
//加密
    @Autowired
    private PasswordEncoder passwordEncoder;
    //直接把USERservice
    @Autowired
    UserService userService;
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("=================>>  执行了认证principalCollection");
//授权操作业务代码
        SimpleAuthorizationInfo sInfo = new SimpleAuthorizationInfo();
           sInfo.addStringPermission("user:add");
        return sInfo;
    }
    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        System.out.println("=================>>  执行了authenticationToken");
        //登录会走到这里  用户名 密码 数据
//链接真实数据
        UsernamePasswordToken token1 = (UsernamePasswordToken) token;
        String username1 = token1.getUsername();
                System.out.println("==================token里面=============>>"+username1);
        SysUser querybyid = userService.querybyname(username1);
        System.out.println("数据库查=====>的用户信息"+querybyid);

        if(querybyid==null){
            System.out.println("没有这个人");
            //没有这个人
            return null;
        }

        String cid = querybyid.getId();
        System.out.println(cid+"拿到的用户id");
//        if (!username.equals(cid)) {
//            System.out.println("=====================>>>>没有这个用户");
//            return null;//意思是会抛出一个异常
//        }
        //密码认证自动做
        System.out.println(querybyid.getPassword()+"数据库拿到的正确密码");
        return  new SimpleAuthenticationInfo("",querybyid.getPassword(),"");
        }
    }



