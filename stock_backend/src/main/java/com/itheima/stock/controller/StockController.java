package com.itheima.stock.controller;

import com.itheima.stock.common.domain.*;
import com.itheima.stock.pojo.StockBlockRtInfo;
import com.itheima.stock.pojo.StockBusiness;
import com.itheima.stock.service.StockService;
import com.itheima.stock.vo.resp.PageResult;
import com.itheima.stock.vo.resp.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author by itheima
 * @Date 2021/12/19
 * @Description
 */
@RestController
@RequestMapping("/api/quot")
@CrossOrigin
public class StockController {

    @Autowired
    private StockService stockService;



    @GetMapping("/stock/business/all")
    public List<StockBusiness> getAllBusiness(){

        return stockService.getAllStockBusiness();
    }


    @GetMapping("/index/all")
    public R<List<InnerMarketDomain>> innerIndexAll() throws ParseException {
        return stockService.innerIndexAll();
    }
    /**
     *需求说明: 沪深两市板块分时行情数据查询，以交易时间和交易总金额降序查询，取前10条数据
     * @return
     */


    @GetMapping("/sector/all")
    public R<List<StockBlockRtInfo>> sectorAll(){


        return stockService.sectorAllLimit();

    }





    /**
     * 沪深两市个股涨幅分时行情数据查询，以时间顺序和涨幅查询前10条数据
     * @return
     */
    @GetMapping("/stock/increase")
    public R<List<StockUpdownDomain>> stockIncreaseLimit(){
        return stockService.stockIncreaseLimit();
    }
    /**
     * 沪深两市个股行情列表查询 ,以时间顺序和涨幅分页查询
     * @param page 当前页
     * @param pageSize 每页大小
     * @return
     */




    @GetMapping("/stock/all")
    public R<PageResult<StockUpdownDomain>> stockPage(Integer page, Integer pageSize){
        return stockService.stockPage(page, pageSize);
    }
    /**
     * 功能描述：沪深两市涨跌停分时行情数据查询，查询T日每分钟的涨跌停数据（T：当前股票交易日）
     * 		查询每分钟的涨停和跌停的数据的同级；
     * 		如果不在股票的交易日内，那么就统计最近的股票交易下的数据
     * 	 map:
     * 	    upList:涨停数据统计
     * 	    downList:跌停数据统计
     * @return
     */
    @GetMapping("/stock/updown/count")
    public R<Map> upDownCount(){
        return stockService.upDownCount();
    }

    @GetMapping("/stock/export")
    public void stockExport(HttpServletResponse response, Integer page, Integer pageSize){
        stockService.stockExport(response,page,pageSize);   //response对象用于向外写出数据
    }
    /**
     * 功能描述：统计国内A股大盘T日和T-1日成交量对比功能（成交量为沪市和深市成交量之和）
     *   map结构示例：
     *      {
     *         "volList": [{"count": 3926392,"time": "202112310930"},......],
     *       "yesVolList":[{"count": 3926392,"time": "202112310930"},......]
     *      }
     * @return
     */
    @GetMapping("/stock/tradevol")
    public R<Map> stockTradeVol4InnerMarket(){
        return stockService.stockTradeVol4InnerMarket();
    }
    /**
     * 查询当前时间下股票的涨跌幅度区间统计功能
     * 如果当前日期不在有效时间内，则以最近的一个股票交易时间作为查询点
     * @return
     */
    @GetMapping("/stock/updown")
    public R<Map> getStockUpDown(){
        return stockService.stockUpDownScopeCount();
    }
    /**
     * 功能描述：查询单个个股的分时行情数据，也就是统计指定股票T日每分钟的交易数据；
     *         如果当前日期不在有效时间内，则以最近的一个股票交易时间作为查询时间点
     * @param code 股票编码
     * @return
     */
    @GetMapping("/stock/screen/time-sharing")
    public R<List<Stock4MinuteDomain>> stockScreenTimeSharing(String code){
        return stockService.stockScreenTimeSharing(code);
    }
    /**
     * 单个个股日K 数据查询 ，可以根据时间区间查询数日的K线数据
     * @param stockCode 股票编码
     */
    @RequestMapping("/stock/screen/dkline")
    public R<List<Stock4EvrDayDomain>> getDayKLinData(@RequestParam("code") String stockCode){
        System.out.println("kkk线被访问");
        return stockService.stockCreenDkLine(stockCode);
    }
//         验证带参数请求是否能访问 无需参数的
    @RequestMapping("/stock/screen/dklinet")
    public void getDayKLinDatat(){
        System.out.println("不带参数kkk线被访问");
    }
    //获取外盘指数
//    @GetMapping("/external/index")
//    public R<List<Stock4EvrDayDomain>>(){
//        return null;
//    }
//通过网络接口 获取国外大盘数据
    @GetMapping("/external/index")
    public R<List<OuterMarketDomain>> outerIndexAll()   {
        System.out.println("我是获取外盘信息我被访问了");
        System.out.println(stockService.outerIndexAll());
        return stockService.outerIndexAll();
    }
//模糊查询 searchStr
    @GetMapping("/stock/search")
    public R<List<FindDomain>> mohuFind(@RequestParam("searchStr") String stockCode){
      //  System.out.println(stockCode+"拿到的code");
        List<FindDomain> findDomains = stockService.serviceMohuFind(stockCode);
//        System.out.println("模糊查询被访问"+stockCode);
//List<FindDomain> list = new ArrayList();
//list.add(new FindDomain("88","jack"));
//        list.add(new FindDomain("89","jack2"));

return R.ok(findDomains);
    }

//    @GetMapping("/stock/search")
//    public R<List<Map>> mohuFind(@RequestParam("searchStr") String stockCode){
//        //  System.out.println(stockCode+"拿到的code");
//        System.out.println("模糊查询被访问"+stockCode);
//      List<Map> list = new ArrayList();
////        list.add(new FindDomain("88","jack"));
////        list.add(new FindDomain("89","jack2"));
//        HashMap<String, String> data0 = new HashMap<>();
//data0.put("code","600000");
//        data0.put("name","白云机场");
//
//        HashMap<String, String> data1 = new HashMap<>();
//        data1.put("code","6000001");
//        data1.put("name","白云机场1");
//
//        HashMap<String, String> data2 = new HashMap<>();
//        data2.put("code","6000002");
//        data2.put("name","白云机场2");
//
//        list.add(data0);
//        list.add(data1);
//        list.add(data2);
//
//        return R.ok(list);
//
//    }

 //接口说明 code
// 功能描述：个股主营业务查询接口
 @GetMapping("/stock/describe")
public R<GuDetail> Gpdetai(@RequestParam("code") String stockCode){
     try {
         GuDetail guDetails = stockService.serviceGPFind(stockCode);
         System.out.println(guDetails.toString());
         System.out.println("收到的code"+stockCode);
         return R.ok(guDetails);
     } catch (Exception e) {
         return R.error("系统升级中");
     }
 }

//    //接口说明 code
//    功能描述：统计每周内的股票数据信息，信息包含：
//    股票ID、 一周内最高价、 一周内最低价 、周1开盘价、周5的收盘价、
//    整周均价、以及一周内最大交易日期（一般是周五所对应日期）;
//    服务路径：/api/quot/stock/screen/weekkline
//    服务方法：GET
//    请求参数：code //股票编码


    @GetMapping("/stock/screen/weekkline")
    public R<List<Stock4EvrWeekDomain>> getweekLinData(@RequestParam("code") String stockCode){
        System.out.println("周k线被访问"+stockCode);
        R<List<Stock4EvrWeekDomain>> listR = stockService.stockCreenWkLine(stockCode);
        return listR;
    }

//    //接口说明
//    功能描述：
//    获取个股最新分时行情数据，主要包含：
//    开盘价、前收盘价、最新价、最高价、最低价、成交金额和成交量、交易时间信息;
//    服务路径：/api/quot/stock/screen/second/detail
//    服务方法：GET
//    请求参数：code //股票编码
//    请求频率：每分钟
@GetMapping("/stock/screen/second/detail")
public R<seconDetailDomain> seconGetail(@RequestParam("code") String stockCode) {
    System.out.println("个股最新分时行情数据接口分析收到的" + stockCode);
    seconDetailDomain seconDetailDomain = stockService.seconGetail(stockCode);
    return R.ok(seconDetailDomain);
}
//功能描述：个股交易流水行情数据查询--查询最新交易流水，按照交易时间降序取前10
//服务路径：/quot/stock/screen/second
//服务方法：GET
//请求频率：5秒

//public R<List<>>


    @GetMapping("/stock/screen/second")
    public R<List<secondDomain>> getsecondData(@RequestParam("code") String stockCode){
        System.out.println("个股实时交易流水查询"+stockCode);
        R<List<secondDomain>> listR = stockService.getsecondData(stockCode);
        return listR;
    }






















}