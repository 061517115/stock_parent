package com.itheima.stock.controller;


import com.google.common.base.Strings;
import com.itheima.stock.common.enums.ResponseCode;
import com.itheima.stock.mapper.SysUserMapper;
import com.itheima.stock.pojo.SysRole;
import com.itheima.stock.pojo.SysUser;
import com.itheima.stock.service.UserService;
import com.itheima.stock.utils.RedisUtil;
import com.itheima.stock.vo.req.LoginReqVo;
import com.itheima.stock.vo.req.LoginZHReqVo;
import com.itheima.stock.vo.req.UpRoleVo;
import com.itheima.stock.vo.resp.LoginRespVo;
import com.itheima.stock.vo.resp.PageResult;
import com.itheima.stock.vo.resp.R;
import com.itheima.stock.vo.resp.roleResult;
import org.apache.ibatis.annotations.Delete;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author by itheima
 * @Date 2021/12/29
 * @Description 定义用户访问层
 */
@RestController
@RequestMapping("/api")
@CrossOrigin
public class UserController {
    @Autowired  //那边放到IOC容器中 ,这边自动注入
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RedisUtil redisUtil;       //拿到工具类  工具类里面自动注入了 temps
    @Autowired
    private UserService userService;
    @Autowired
    private SysUserMapper sysUserMapper;
    /**
     * 用户登录功能实现
     * @param vo
     * @return
     */
    @PostMapping("/login")
    public R<LoginRespVo> login(@RequestBody LoginReqVo vo){
        System.out.println("被访问了");
        //从redis中拿到数据
        String rcode = (String)redisUtil.get(vo.getRkey());
        System.out.println(rcode+"redis拿到的");
//校验
        if (Strings.isNullOrEmpty(rcode) || !rcode.equals(vo.getCode())) {
            return R.error("验证码错误");
        }
        System.out.println("验证码验证痛过");
        String username = vo.getUsername();
        String password = vo.getPassword();
        SysUser fuser=this.sysUserMapper.findByUserName(vo.getUsername());
        String password1 = fuser.getPassword();   //数据库拿到密码
        System.out.println("数据库拿到的密码"+password1);
        System.out.println("前端传来的密码"+password);
        boolean matches = passwordEncoder.matches(password, password1);
        System.out.println(matches+"判断是否一致");
        String encodePassword = passwordEncoder.encode(password);
        System.out.println(encodePassword+"加密后的密码");
        boolean matches1 = passwordEncoder.matches("123456", encodePassword);
        System.out.println("加密后是否一致"+matches1);
        Subject subject = SecurityUtils.getSubject();            //shiro安全对象
        UsernamePasswordToken utoken = new UsernamePasswordToken(username, password);
        try {
            subject.login(utoken);
           //组装登录成功数据
            SysUser user=this.sysUserMapper.findByUserName(vo.getUsername());
            LoginRespVo respVo = new LoginRespVo();
            //属性名称与类型必须相同，否则copy不到
            BeanUtils.copyProperties(user,respVo);
            return  R.ok(respVo);
        } catch (AuthenticationException e) {
            System.out.println("密码验证不通过");
            ResponseCode dataError = ResponseCode.DATA_ERROR;
            String message = dataError.getMessage();
            System.out.println(message+"应该拿到的消息");
            return R.error(ResponseCode.SYSTEM_PASSWORD_ERROR.getMessage());
        }
    }
    /**
     * 生成验证码
     *  map结构：
     *      code： xxx,
     *      rkey: xxx
     * @return
     */
    @GetMapping("/captcha")
    public R<Map> generateCaptcha(){
        return this.userService.generateCaptcha();


    }

    @PostMapping("/users")
public R<PageResult>   loginZHReqVo(@RequestBody LoginZHReqVo loginZHReqVo){
    System.out.println("测试拿到的"+loginZHReqVo.toString());
        R<PageResult> zhusers = userService.Zhusers(loginZHReqVo);
        System.out.println(zhusers.toString());
        return  zhusers;
}

    @PostMapping("/user")
public R<String>  addUser(@RequestBody SysUser sysUser){
        System.out.println(sysUser.toString());
        try {
            userService.addUser(sysUser);
            return R.ok("添加成功");
        } catch (Exception e) {
           return R.error("失败,系统繁忙中");
        }
    }
    //角色管理

    @GetMapping("/user/roles/{userId}")
    public R<roleResult> rolesMange(@PathVariable String userId){
        System.out.println("收到的ID"+userId);

        List<String> strings = userService.rolesManage(userId);  //特有id

        List<SysRole> allrole = userService.findAllrole();//公共角色库

        roleResult r = new roleResult();
r.setOwnRoleIds(strings);
r.setAllRole(allrole);
        return R.ok(r);
    }
//更新用户角色信息
    @PutMapping("/user/roles")
    public R<String> upDateRole(@RequestBody UpRoleVo upRoleVo){
        System.out.println("收到的"+upRoleVo.toString());

            userService.updateRole(upRoleVo);
            return R.ok("KO");
    }
//删除用户

    @DeleteMapping("/user")
    public R<String> deletUser(@RequestBody List<Long> userIds){
        System.out.println("收到的id"+userIds.toString());
        if(!userIds.isEmpty()){
        for (int i = 0; i < userIds.size(); i++) {
            userService.swManage(userIds.get(i));
        }}
        return  R.ok("删除成功");

    }

}