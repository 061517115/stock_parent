package com.itheima.stock.mapper;

import com.itheima.stock.pojo.RoleID;
import com.itheima.stock.pojo.SysRole;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Date;
import java.util.List;

/**
* @author jack
* @description 针对表【sys_role(角色表)】的数据库操作Mapper
* @createDate 2022-03-09 11:05:14
* @Entity com.itheima.stock.pojo.SysRole
*/
public interface SysRoleMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

    //sys_user_role 根据用户id 查询用户角色id
    @Select("SELECT sys_user_role.role_id as rid FROM sys_user_role WHERE sys_user_role.user_id = #{uid}")
    List<String> findRoleid(@Param("uid")String uid);

    @Select("SELECT * FROM sys_role")
    List<SysRole> findallrole();

    //添加id
    void updateRole(@Param("uid") String uid, @Param("roleIDs") List<RoleID> roleIDs, @Param("date1") String date);
//    删除用户角色表
    @Delete("DELETE FROM sys_user_role WHERE sys_user_role.user_id = #{uid}")
    void  deletRole (@Param("uid") String uid);



}
