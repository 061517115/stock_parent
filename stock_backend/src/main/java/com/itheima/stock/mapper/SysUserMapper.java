package com.itheima.stock.mapper;

import com.itheima.stock.pojo.SysUser;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
* @author jack
* @description 针对表【sys_user(用户表)】的数据库操作Mapper
* @createDate 2022-03-09 11:05:14
* @Entity com.itheima.stock.pojo.SysUser
*/
public interface SysUserMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysUser record);

    int updateByPrimaryKey(SysUser record);

    SysUser findByUserName(@Param("username") String username);

    /*
        1.username=#{username} 等号左边的username数据表的字段名  等号右边大括号中的username看此时login登录方法的形参类型
        这里login登录方法形参类型是User类型，如果形参是一个pojo属于复杂类型，看实体类User中的getXxxx()去掉get，X变为x
        例如getUsername----username,#{username}在mybatis底层调用的是User实体类中的getUsername
     */

    @Select("select * from sys_user where username=#{username} and password=#{password}")
    public abstract SysUser login(SysUser user);


    @Select("select * from sys_user where username = #{username}")
    SysUser querybyid(@Param("username")String username);

    @Select("select * from sys_user ")
    List<SysUser> Zhusers();

    /*
     *   查询总数
     * */
    @Select("SELECT count(*) FROM sys_user")
    int findSum();

    //添加用户
     void  addUser(SysUser user);
//删除用户
    @Delete("DELETE FROM sys_user WHERE sys_user.id = #{id}")
    void deletUser(Long id);
    //用户角色表也需要删除

    @Delete("DELETE FROM sys_user_role WHERE sys_user_role.user_id = #{id}")
    void  deletRu(Long id);

}
