package com.itheima.stock.service;

public interface StockTimerService {
    /**
     * 定义获取分钟级股票数据
     */
    void getStockRtIndex();

    void getStockSectorRtIndex();
}
