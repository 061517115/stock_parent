package com.itheima.stock.service;

import java.util.List;

public interface StockTimerTaskService {
    /**
     * 获取国内大盘的实时数据信息
     */
    void getInnerMarketInfo();

    List<String> getStockIds();

    void getOuterMarketInfo();
}