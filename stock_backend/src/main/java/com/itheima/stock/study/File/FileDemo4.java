package com.itheima.stock.study.File;
import java.io.File;

/*
    File类判断和获取功能
        public boolean isDirectory()   测试此抽象路径名表示的File是否为目录
        public boolean isFile()    测试此抽象路径名表示的File是否为文件
        public boolean exists()    测试此抽象路径名表示的File是否存在
        public String getAbsolutePath()    返回此抽象路径名的绝对路径名字符串
        public String getPath()    将此抽象路径名转换为路径名字符串
        public String getName()    返回由此抽象路径名表示的文件或目录的名称
 */
public class FileDemo4 {
    public static void main(String[] args) {
        File f1 = new File("day10_demo\\aaa");
        File f2 = new File("day10_demo\\a.txt");

//        public boolean isDirectory() 测试此抽象路径名表示的File是否为目录
        System.out.println(f1.isDirectory());
        System.out.println(f2.isDirectory());


//        public boolean isFile()  测试此抽象路径名表示的File是否为文件
        System.out.println(f1.isFile());
        System.out.println(f2.isFile());

        System.out.println("==================================================");
        //        public boolean exists()  测试此抽象路径名表示的File是否存在
        System.out.println(f1.exists());


    }
}
