package com.itheima.stock.study.day03;

import java.util.ArrayList;

public class StreamDemo {

/*
    体验Stream流的好处
    需求：按照下面的要求完成集合的创建和遍历
        1 创建一个集合，存储多个字符串元素
            "张无忌" , "张翠山" , "张三丰" , "谢广坤" , "赵四" , "刘能" , "小沈阳" , "张良"
        2 把集合中所有以"张"开头的元素存储到一个新的集合
        3 把"张"开头的集合中的长度为3的元素存储到一个新的集合
        4 遍历上一步得到的集合
 */
public static void main(String[] args) {
    // 传统方式完成
    ArrayList<String> list = new ArrayList<>();
    list.add("张无忌");
    list.add("张翠山");
    list.add("张三丰");
    list.add("谢广坤");
    list.add("赵四");
    list.add("刘能");
    list.add("小沈阳");
    list.add("张良");
    // 把集合中所有以"张"开头的元素存储到一个新的集合
    ArrayList<String> list2 = new ArrayList<>();
    for (String s : list) {
        if (s.startsWith("张")) {
            list2.add(s);
        }
    }
    // 把"张"开头的集合中的长度为3的元素存储到一个新的集合
    ArrayList<String> list3 = new ArrayList<>();
    for (String s : list2) {
        if (s.length() == 3) {
            list3.add(s);
        }
    }
    // 遍历list3集合
    for (String s : list3) {
        System.out.println(s);
    }

    System.out.println("================上面传统方式================");
          //第一步获取流对象
    list.stream().filter(s -> s.startsWith("张")&& s.length()==3).forEach(s -> System.out.println(s));







}





}
