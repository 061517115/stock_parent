package com.itheima.stock.study.day03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class StreamDemo2 {
    /*
    Stream流中三类方法之一 :  获取方法

    1 单列集合
       可以使用Collection接口中的默认方法stream()生成流
       default Stream<E> stream()
    2 双列集合
        双列集合不能直接获取 , 需要间接的生成流
        可以先通过keySet或者entrySet获取一个Set集合，再获取Stream流
    3 数组
       Arrays中的静态方法stream生成流
 */
    public static void main(String[] args) {
        method2();
    }
    // 单列集合的获取
    // method1();
    private static void method3() {
        int[] arr = {1, 2, 3, 4, 5, 6};
        Arrays.stream(arr).forEach(s -> System.out.println(s));  //用这个生成流 必须把 集合放进去(其实也可以用集合对象.steam获取)
    }
// 双列集合的获取
    // method2();
private static void method2() {
    HashMap<String, String> hm = new HashMap<>();
    hm.put("it001", "曹植");
    hm.put("it002", "曹丕");
    hm.put("it003", "曹熊");
    hm.put("it004", "曹冲");
    hm.put("it005", "曹昂");

    // 获取map集合的健集合 , 在进行输出打印  ntrySet 本身就是键值对
    hm.keySet().stream().forEach(s -> System.out.println(s+hm.get(s)));
    System.out.println("下面是entrySet对象");
    hm.entrySet().stream().forEach(s -> System.out.println(s));
}


    private static void method1() {
        // 可以使用Collection接口中的默认方法stream()生成流
        ArrayList<String> list = new ArrayList<>();
        list.add("迪丽热巴");
        list.add("古力娜扎");
        list.add("马尔扎哈");
        list.add("欧阳娜娜");
        list.stream().forEach(s -> System.out.println(s));
    }










}
