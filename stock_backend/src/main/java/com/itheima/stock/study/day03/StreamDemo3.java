package com.itheima.stock.study.day03;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/*
    Stream流中三类方法之一 :  中间方法

    1 Stream<T> filter(Predicate predicate)：用于对流中的数据进行过滤
        Predicate接口中的方法 : boolean test(T t)：对给定的参数进行判断，返回一个布尔值
    2 Stream<T> limit(long maxSize)：截取指定参数个数的数据
    3 Stream<T> skip(long n)：跳过指定参数个数的数据
    4 static <T> Stream<T> concat(Stream a, Stream b)：合并a和b两个流为一个流
    5 Stream<T> distinct()：去除流中重复的元素。依赖(hashCode和equals方法)
    6 Stream<T> sorted () : 将流中元素按照自然排序的规则排序
    7 Stream<T> sorted (Comparator<? super T> comparator) : 将流中元素按照自定义比较器规则排序

 */
public class StreamDemo3 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("张无忌");
        list.add("张翠山");
        list.add("张三丰");
        list.add("谢广坤");
        list.add("赵四8888");
        list.add("刘能");
        list.add("小沈阳");
        list.add("张良");
        list.add("张良");
        list.add("张良");
        list.add("张良888");
        //  // Stream<T> limit(long maxSize)：截取指定参数个数的数据
      //  list.stream().limit(1).forEach(s -> System.out.println(s));

        // Stream<T> skip(long n)：跳过指定参数个数的数据   跳过前三个
        //list.stream().skip(3).forEach(s-> System.out.println(s));

   // Stream<T> distinct()：去除流中重复的元素。依赖(hashCode和equals方法)  普通类需要重写java的 hashcode 和 equals方法
      list.stream().distinct().forEach(s->{System.out.println(s);});

        // Stream<T> sorted (Comparator<? super T> comparator) : 将流中元素按照自定义比较器规则排序
        method3(list);
        method3();
        method1(list);
    }
    private static void method3(ArrayList<String> list) {
        //也可以去类中重写 competerfangfa1
        list.stream().sorted(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length() - o2.length();
            }
        }).forEach(s->{
            System.out.println(s);
        });

    }
    // Stream<T> sorted () : 将流中元素按照自然排序的规则排序
    private static void method3() {
        ArrayList<Integer> list2 = new ArrayList<>();
        list2.add(3);
        list2.add(1);
        list2.add(2);
        list2.add(999);
        list2.stream().sorted().forEach(s->{
            System.out.println(s);
        });
    }

//     Stream<T> filter(Predicate predicate)：用于对流中的数据进行过滤
    private static void method1(ArrayList<String> list) {
        // filter方法会获取流中的每一个数据
        // s就代表的是流中的每一个数据
        // 如果返回值为true , 那么代表的是数据留下来
        // 如果返回值的是false , 那么代表的是数据过滤掉
//        list.stream().filter(new Predicate<String>() {
//            @Override
//            public boolean test(String s) {
//                boolean result = s.startsWith("张");
//                return result;
//            }
//        }).forEach(s -> System.out.println(s));

        list.stream().filter(s ->
                s.startsWith("张")           //只有符合条件的才会继续保留再 流中
        ).forEach(s -> System.out.println(s));
    }

}
