package com.itheima.stock.study.day03;

import java.util.ArrayList;
import java.util.function.Consumer;

/*
    Stream流中三类方法之一 :  终结方法
    1 void forEach(Consumer action)：对此流的每个元素执行操作
        Consumer接口中的方法 void accept(T t)：对给定的参数执行此操作
    2 long count()：返回此流中的元素数
 */
public class StreamDemo4 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("张无忌");
        list.add("张翠山");
        list.add("张三丰");
        list.add("谢广坤");
        ArrayList<String> list2 = new ArrayList<>();
        // long count()：返回此流中的元素数
//        long count = list.stream().count();
//        System.out.println(count);
        method1(list,list2);
    }
    // void forEach(Consumer action)：对此流的每个元素执行操作
    private static void method1(ArrayList<String> list,ArrayList<String> list2) {

        list.stream().forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {

                //这里对接收的数据进行操作
                list2.add(s);
                System.out.println(s+list2.size());
            }
        });




        System.out.println("=====================");

        list.stream().forEach( (String s) -> {
            System.out.println(s);
        });

        System.out.println("=====================");

        list.stream().forEach(  s -> { System.out.println(s); });
    }




}


