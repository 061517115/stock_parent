package com.itheima.stock.study.day03;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*

    Stream流的收集操作 : 第二部分

    使用Stream流的方式操作完毕之后，我想把流中的数据起来，该怎么办呢？

    Stream流的收集方法
    R collect(Collector collector) : 此方法只负责收集流中的数据 , 创建集合添加数据动作需要依赖于参数

    工具类Collectors提供了具体的收集方式
    public static <T> Collector toList()：把元素收集到List集合中
    public static <T> Collector toSet()：把元素收集到Set集合中
    public static  Collector toMap(Function keyMapper,Function valueMapper)：把元素收集到Map集合中


    需求 :
        定义一个集合，并添加一些整数1，2，3，4，5，6，7，8，9，10
        将集合中的奇数删除，只保留偶数。
        遍历集合得到2，4，6，8，10
 */
public class StreamDemo6 {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            list.add(i);
        }
        list.add(10);
        list.add(10);
        list.add(10);
        list.add(10);
        list.add(10);

        // collect只负责收集流中的数据       collect (这里面需要传参数,想要变成何种集合就看这个参数)
        // Collectors.toList()会负责在底层创建list集合 ,并把数据添加到集合中 , 返回集合对象
        List<Integer> list2 = list.stream().filter(num -> num % 2 == 0 ).collect(Collectors.toList());
        System.out.println(list2);

        Set<Integer> set = list.stream().filter(num -> num % 2 == 0 ).collect(Collectors.toSet());
        System.out.println(set);

    }
}