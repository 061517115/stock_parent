package com.itheima.stock.vo.req;

import lombok.Data;

/**
 * @author by itheima
 * @Date 2021/12/30
 * @Description 登录请求vo
 */
@Data
public class LoginZHReqVo {
    /**
     * 当前页数
     */
    private String pageNum;  //赋予前端对象

    private String   pageSize;// 页面大小

    /**
     * 用户名
     */
    private String username;
    /**
     * 别名
     */
    private String nickName;
    /**
     * 开始时间
     */
    private String startTime;



    /**
     * 结束时间
     */
    private String endTime;







}