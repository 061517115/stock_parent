package com.itheima.stock.vo.req;

import lombok.Data;

import java.util.List;

/**
 * @author by itheima
 * @Date 2021/12/30
 * @Description 登录请求vo
 */
@Data
public class UpRoleVo {
    /**
     * 用户名
     */
    private String userId;
    /**
     * 密码
     */
    private List<String> roleIds;



}